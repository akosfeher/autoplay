package hu.itbt.autoplay;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.json.JSONException;

import hu.itbt.autoplay.model.AutoPlayer;

@WebListener
public class StartupListener implements ServletContextListener {
//	@Inject

	protected static String CEGID = ApplicationConfig.getInstance().getProperty("CEGID");

	static {
		if (CEGID == null || CEGID.isEmpty()) {
			CEGID = "softic";
		}
	}

//	protected static UserRepo userRepo;
//
//	protected static UserRepo getUserRepo() {
//		if (userRepo == null) {
//			userRepo = new UserRepo();
//		}
//		return userRepo;
//	}

	protected static String g4tApiUrl;

	protected static boolean cronServer;

	public static ScheduledThreadPoolExecutor schedulerC;

	private static Client client;
	private static Map<String, WebTarget> target = new HashMap<>();

	private static Map<String, AutoPlayer> players = new HashMap<>();

	@PostConstruct
	protected void init() {
		System.out.println("-------------------------  postconstruct inited");
	}

	public static void move(String gameId, long playerId, int x, int y) throws JSONException, URISyntaxException {
		AutoPlayer pl = players.get(gameId + "-" + playerId);
		if (pl != null) {
			pl.move(x, y);
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {

		g4tApiUrl = ApplicationConfig.getInstance().getProperty("G4T_API_URL");
		client = ClientBuilder.newClient();

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		if (schedulerC != null) {
			schedulerC.shutdownNow();
		}
		ServletContext sc = sce.getServletContext();
		sc.removeAttribute("path");
		sc.removeAttribute("mode");
		System.out.println("Value deleted from context.");
	}
}