package hu.itbt.autoplay;
// import the rest service you created!

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import hu.itbt.autoplay.pages.IndexPage;
import hu.itbt.autoplay.rest.AdminRest;
import hu.itbt.autoplay.rest.CalendarRest;
import hu.itbt.autoplay.rest.RobotRest;
import hu.itbt.autoplay.rest.UsersRest;
import hu.itbt.autoplay.utils.CORSFilter;
import hu.itbt.autoplay.utils.JWTRoleNeededFilter;

@ApplicationPath("/rest")
public class RESTApplication extends Application {

	public RESTApplication() {
	}

	/**
	 * Gets the classes.
	 *
	 * @return the classes
	 */
	@Override
	public Set<Class<?>> getClasses() {
		final Set<Class<?>> classes = new HashSet<>();

		classes.add(JWTRoleNeededFilter.class);
		classes.add(CORSFilter.class);

		classes.add(UsersRest.class);
		classes.add(CalendarRest.class);
		classes.add(RobotRest.class);
		classes.add(AdminRest.class);

		classes.add(IndexPage.class);

		return classes;
	}
}