package hu.itbt.autoplay.service;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Named;

import com.google.gson.JsonObject;

@Named
public class LogEvent extends AbstractRepo {

	@Override
	protected String tableName() {
		return "logs";
	}

	@Override
	protected String sequenceName() {
		return "logs_id_seq";
	}

	@Override
	protected Set<String> leftoutColumns() {
		Set<String> cols = new HashSet<>();
		return cols;
	}

	public boolean addEvent(int status, String message, String cegId, LogEventTypesInterface eventType, Long userId,
			String role, String data) {
		try {
			JsonObject obj = new JsonObject();
			if (eventType == null) {
				return false;
			}
			obj.addProperty("type", eventType.name());
			obj.addProperty("user_id", userId);
			obj.addProperty("status", status);
			obj.addProperty("message", message);
			obj.addProperty("role", role);
			obj.addProperty("data", data);
			Identifiable root = new Identifiable(obj);
			String id = save(cegId, root);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
