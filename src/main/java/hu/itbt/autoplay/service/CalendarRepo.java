package hu.itbt.autoplay.service;

import java.util.HashSet;
import java.util.Set;

public class CalendarRepo extends AbstractRepo {

	@Override
	protected String tableName() {
		return "CALENDAR";
	}

	@Override
	protected String sequenceName() {
		return "calendar_id_seq";
	}

	@Override
	protected Set<String> leftoutColumns() {
		Set<String> cols = new HashSet<>();
		return cols;
	}

}
