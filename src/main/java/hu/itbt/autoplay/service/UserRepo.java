package hu.itbt.autoplay.service;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import hu.itbt.autoplay.model.User;

@ApplicationScoped
@Named
@ManagedBean
public class UserRepo extends UserBaseRepo {
	@Override
	protected String tableName() {
		return "users";
	}

	@Override
	@PostConstruct
	public void init() {
		System.out.println("UserRepo created");
	}

	@Override
	protected String sequenceName() {
		return "users_id_seq";
	}

	public byte[] getPortrait(String cegId, long userId) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.getPortrait(connection, cegId, userId);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private byte[] getPortrait(Connection connection, String cegId, long userId) {
		String sql = "select portrait from  " + cegId + ".users  where id = ?";

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(sql);

			ps.setLong(1, Long.valueOf(userId));

			rs = ps.executeQuery();

			byte[] portrait = null;
			if (rs.next()) {
				portrait = rs.getBytes(1);
			}

			return portrait;
		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String updatePortrait(String cegId, long userId, byte[] bytes) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.updatePortrait(connection, cegId, userId, bytes);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private String updatePortrait(Connection connection, String cegId, long userId, byte[] bytes) {
		String sql = "update " + cegId + ".users set portrait = ? where id = ?";

		PreparedStatement ps = null;

		try {
			ps = connection.prepareStatement(sql);
			ps.setBytes(1, bytes);
			ps.setLong(2, Long.valueOf(userId));

			ps.executeUpdate();

			return "done";
		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public long addAsTst(String cegId, String name, String email) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.addAsTst(connection, cegId, name, email);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private long addAsTst(Connection connection, String cegId, String name, String email) {
		String sql = "insert into " + cegId
				+ ".users (email, name, ceg, active, hash, salt, role) select ?, ?, ceg, active, hash, salt, role "
				+ " from " + cegId + ".users where email = 'tst9@tst.com' ";

		PreparedStatement ps = null;

		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, email);
			ps.setString(2, name);

			long id = ps.executeUpdate();

			return id;
		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String getFreePlayers(String cegId) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.getFreePlayers(connection, cegId);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find user, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public String getFreePlayers(Connection connection, String cegId) {
		String sql = "select * from " + (cegId + "." + this.tableName())
				+ " where active_game_id < 1 or active_game_id is null ";

		PreparedStatement statement = null;

		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(sql);

			return executeQueryToJson(statement, resultSet, false, false);
		} catch (Exception e) {
			return "";
		} finally {
			this.close(resultSet);
			this.close(statement);
		}
	}

	public List<User> getRobots(String cegId) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.getRobots(connection, cegId);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find user, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public List<User> getRobots(Connection connection, String cegId) {
		String sql = "select * from " + (cegId + "." + this.tableName())
				+ " where active_game_id > 1 and email like 'robot%' ";

		PreparedStatement statement = null;

		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(sql);

			String js = executeQueryToJson(statement, resultSet, false, false);

			Gson gson = new Gson();

			Type listType = new TypeToken<ArrayList<User>>() {
			}.getType();
			List<User> users = gson.fromJson(js, listType);

			return users;

		} catch (Exception e) {
			return new ArrayList<User>();
		} finally {
			this.close(resultSet);
			this.close(statement);
		}
	}

}
