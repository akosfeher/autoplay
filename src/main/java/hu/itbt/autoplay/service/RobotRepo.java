package hu.itbt.autoplay.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import hu.itbt.autoplay.model.AutoPlayer;
import hu.itbt.autoplay.model.RobotBoard;
import hu.itbt.autoplay.model.User;

public class RobotRepo {

	private static Logger logger = Logger.getLogger(RobotRepo.class.getName());

	protected static UserRepo userRepo;

	protected static UserRepo getUserRepo() {
		if (userRepo == null) {
			userRepo = new UserRepo();
		}
		return userRepo;
	}

	protected static Map<Long, RobotBoard> robotBoards = new HashMap<>();

	protected static Map<Long, Map<Long, AutoPlayer>> robotMap = new HashMap<>();

	public void createActiveGameRobots(String cegId, long activeGameId, RobotBoard rb) {
		// If exist, stop all and remove from the map
		Map<Long, AutoPlayer> robots = robotMap.get(activeGameId);

		if (robots != null && robots.size() > 0) {
			for (AutoPlayer auto : robots.values()) {
				if (auto != null && auto.isAlive()) {
					auto.interrupt();
				}
			}
			robotMap.remove(activeGameId);
		}

		List<User> robotUsers = getUserRepo().getRobots(cegId);
		// Collections.shuffle(robotUsers);

		Map<Long, AutoPlayer> activeRobots = new HashMap<>();
		for (User u : robotUsers) {
			if (activeGameId != u.getActive_game_id() || u.getForm() == null || u.getForm().isEmpty()) {
				continue;
			}

			AutoPlayer player1;
			try {
				player1 = new AutoPlayer(u.getName(), u, rb);
				if (!player1.isInterrupted()) {

					player1.start();

					activeRobots.put(u.getId(), player1);

					Thread.sleep((long) (100 + Math.random() * 100));
				} else {
					logger.info(player1.getName() + " robot player interrupted");
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		robotMap.put(activeGameId, activeRobots);

	}

//	public void activeGameEnded(String cegId, long activeGameId) {
//		// If exist, stop all and remove from the map
//		Map<Long, AutoPlayer> robots = robotMap.get(activeGameId);
//
//		if (robots != null && robots.size() > 0) {
//			for (AutoPlayer auto : robots.values()) {
//				if (auto != null && auto.isAlive()) {
//					//auto.getSocket().disconnect();
//					auto.interrupt();
//				}
//			}
//			robotMap.remove(activeGameId);
//		}
//	}

	public Map<Long, Map<Long, AutoPlayer>> getRobotMap() {
		return robotMap;
	}

	public RobotBoard createRobotBoard(String cegId, long activeGameId) {
		// first cleanup previous if exist
		if (robotBoards.containsKey(activeGameId)) {
			RobotBoard rb = robotBoards.get(activeGameId);
			robotBoards.remove(activeGameId);
			try {
				rb.getSocket().disconnect();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			rb = null;
		}

		RobotBoard rb = new RobotBoard(cegId, activeGameId);

		robotBoards.put(activeGameId, rb);

		return rb;
	}

	public void removeRobotBoards(String cegId) {

		for (RobotBoard rb : robotBoards.values()) {
			if (rb.getSocket() != null) {
				rb.getSocket().disconnect();
			}
			rb = null;
		}

		robotBoards.clear();
	}

	public void removeRobots(String cegId) {
		for (Map<Long, AutoPlayer> robots : robotMap.values()) {

			if (robots != null && robots.size() > 0) {
				for (AutoPlayer auto : robots.values()) {
					if (auto != null && auto.isAlive()) {
						// auto.getSocket().disconnect();
						auto.interrupt();
					}
				}
			}
			robots.clear();
		}
		robotMap.clear();
	}

	public void removeGameRobotBoards(String cegId, long activeGameId) {

		RobotBoard rb = robotBoards.get(activeGameId);

		if (rb != null && rb.getSocket() != null) {
			rb.getSocket().disconnect();
		}
		rb = null;

		robotBoards.remove(activeGameId);
	}

	public void removeActivegameRobots(String cegId, long activeGameId) {
		Map<Long, AutoPlayer> robots = robotMap.get(activeGameId);

		if (robots != null && robots.size() > 0) {
			for (AutoPlayer auto : robots.values()) {
				if (auto != null && auto.isAlive()) {
					// auto.getSocket().disconnect();
					auto.interrupt();
				}
			}
			robots.clear();
		}

		robotMap.remove(activeGameId);
	}
}
