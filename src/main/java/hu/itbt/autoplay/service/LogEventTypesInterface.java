package hu.itbt.autoplay.service;

public interface LogEventTypesInterface {

	public String name();

	public String getDesc();

	public String getMode();

	public String getSubsystem();

	public static LogEventTypesInterface valueOf(String name) {
		return null;
	}

}
