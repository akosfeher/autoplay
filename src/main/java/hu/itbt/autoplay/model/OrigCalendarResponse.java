package hu.itbt.autoplay.model;

import java.util.Date;
import java.util.List;

public class OrigCalendarResponse {
	protected int type;
	protected int size;
	protected boolean fill;
	protected int minimumSize;
	protected boolean repeatCovers;
	protected boolean listTimes;
	protected boolean eventsOutside;
	protected boolean updateRows;
	protected boolean updateColumns;

	protected long around;

	protected List<EventEntry> events;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isFill() {
		return fill;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}

	public int getMinimumSize() {
		return minimumSize;
	}

	public void setMinimumSize(int miniumSize) {
		this.minimumSize = miniumSize;
	}

	public boolean isRepeatCovers() {
		return repeatCovers;
	}

	public void setRepeatCovers(boolean repeatCovers) {
		this.repeatCovers = repeatCovers;
	}

	public boolean isListTimes() {
		return listTimes;
	}

	public void setListTimes(boolean listTimes) {
		this.listTimes = listTimes;
	}

	public boolean isEventsOutside() {
		return eventsOutside;
	}

	public void setEventsOutside(boolean eventsOutside) {
		this.eventsOutside = eventsOutside;
	}

	public boolean isUpdateRows() {
		return updateRows;
	}

	public void setUpdateRows(boolean updateRows) {
		this.updateRows = updateRows;
	}

	public boolean isUpdateColumns() {
		return updateColumns;
	}

	public void setUpdateColumns(boolean updateColumns) {
		this.updateColumns = updateColumns;
	}

	public long getAround() {
		return around;
	}

	public void setAround(long around) {
		this.around = around;
	}

	public List<EventEntry> getEvents() {
		return events;
	}

	public void setEvents(List<EventEntry> events) {
		this.events = events;
	}

	public OrigCalendarResponse(List<EventEntry> events) {
		super();
		this.events = events;
		type = 1;
		size = events.size();
		minimumSize = 0;
		fill = false;
		repeatCovers = true;
		listTimes = true;
		eventsOutside = true;
		updateRows = true;
		updateColumns = true;
		around = (new Date()).getTime(); // LocalDateTime.now().toEpochSecond(null) * 1000;
	}

}
