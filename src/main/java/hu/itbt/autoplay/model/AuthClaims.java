package hu.itbt.autoplay.model;

import java.io.Serializable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class AuthClaims implements Serializable, Jsonable {

	private static final long serialVersionUID = 3753564029327443312L;

	protected String jti;
	protected String ceg;
	protected String role;
	protected String sub;
	protected String email;
	protected String name;
	protected String username;

	public AuthClaims(String id, String ceg, String role, String subject, String email, String name, String username) {
		this.jti = id;
		this.ceg = ceg;
		this.role = role;
		this.sub = subject;
		this.email = email;
		this.name = name;
		this.username = username;
	}

	public AuthClaims() {
	}

	public String getId() {
		return jti;
	}

	public String getCeg() {
		return ceg;
	}

	public String getRole() {
		return role;
	}

	public String getSubject() {
		return sub;
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	public String getUsername() {
		return username;
	}

	@Override
	public String toString() {
		return "AuthClaims{" + "id='" + jti + '\'' + ", ceg='" + ceg + '\'' + ", role='" + role + '\'' + ", subject='"
				+ sub + '\'' + ", email='" + email + '\'' + ", name='" + name + '\'' + '}';
	}

	@Override
	public JsonObject getJsonObject() {
		JsonObject json = new JsonObject();
		json.addProperty("id", jti);
		json.addProperty("ceg", ceg);
		json.addProperty("role", role);
		json.addProperty("sub", sub);
		json.addProperty("email", email);
		json.addProperty("name", name);
		json.addProperty("username", username);
		return json;
	}

	@Override
	public String getJsonString() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJson = gson.toJson(getJsonObject());
		return prettyJson;
	}

}
