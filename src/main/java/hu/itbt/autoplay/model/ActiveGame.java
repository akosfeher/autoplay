package hu.itbt.autoplay.model;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActiveGame {

	private Long playerId;
	private Long gameId;
	private Player player;
	private Long numOfTeams;
	private List<Team> teams = null;
	private Long day;
	private List<Poziciok> poziciok = null;
	private Points points;
	private String dayEnds;

	public String getDayEnds() {
		return dayEnds;
	}

	public void setDayEnds(String dayEndsStr) {
		this.dayEnds = dayEndsStr;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Long getNumOfTeams() {
		return numOfTeams;
	}

	public void setNumOfTeams(Long numOfTeams) {
		this.numOfTeams = numOfTeams;
	}

	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public Long getDay() {
		return day;
	}

	public void setDay(Long day) {
		this.day = day;
	}

	public List<Poziciok> getPoziciok() {
		return poziciok;
	}

	public void setPoziciok(List<Poziciok> poziciok) {
		this.poziciok = poziciok;
	}

	public Points getPoints() {
		return points;
	}

	public void setPoints(Points points) {
		this.points = points;
	}

	public long secondsToDayend() {
		String[] st = dayEnds.split(",");
		LocalDateTime dayEnd = LocalDateTime.parse(st[this.day.intValue() - 1].trim() + ":00");
		LocalDateTime now = LocalDateTime.now();

		long seconds = now.until(dayEnd, ChronoUnit.SECONDS);

		return seconds;
	}

	@Override
	public String toString() {
		return "ActiveGame [playerId=" + playerId + ", gameId=" + gameId + ", player=" + player + ", numOfTeams="
				+ numOfTeams + ", teams=" + teams + ", day=" + day + ", poziciok=" + poziciok + ", points=" + points
				+ ", dayEnds=" + dayEnds + "]";
	}

	public static class Player {

		private Long id;
		private String name;
		private Long gameId;
		private int teamIndex;
		private int playerIndex;
		private String form;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Long getGameId() {
			return gameId;
		}

		public void setGameId(Long gameId) {
			this.gameId = gameId;
		}

		public int getTeamIndex() {
			return teamIndex;
		}

		public void setTeamIndex(int teamIndex) {
			this.teamIndex = teamIndex;
		}

		public int getPlayerIndex() {
			return playerIndex;
		}

		public void setPlayerIndex(int playerIndex) {
			this.playerIndex = playerIndex;
		}

		public String getForm() {
			return form;
		}

		public void setForm(String form) {
			this.form = form;
		}

		@Override
		public String toString() {
			return "Player [id=" + id + ", name=" + name + ", gameId=" + gameId + ", teamIndex=" + teamIndex
					+ ", playerIndex=" + playerIndex + ", form=" + form + "]";
		}

	}

	public class Poziciok {

		private int x;
		private int y;
		private String tp;
		private int day;

		public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			this.y = y;
		}

		public String getTp() {
			return tp;
		}

		public void setTp(String tp) {
			this.tp = tp;
		}

		public int getDay() {
			return day;
		}

		public void setDay(int day) {
			this.day = day;
		}

		@Override
		public String toString() {
			return "Poziciok [x=" + x + ", y=" + y + ", tp=" + tp + ", day=" + day + "]";
		}

	}

	public static class Team {

		private Long teamIndex;
		private Long numOfPlayers;
		private List<Player> players = null;
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();

		public Long getTeamIndex() {
			return teamIndex;
		}

		public void setTeamIndex(Long teamIndex) {
			this.teamIndex = teamIndex;
		}

		public Long getNumOfPlayers() {
			return numOfPlayers;
		}

		public void setNumOfPlayers(Long numOfPlayers) {
			this.numOfPlayers = numOfPlayers;
		}

		public List<Player> getPlayers() {
			return players;
		}

		public void setPlayers(List<Player> players) {
			this.players = players;
		}

		public Map<String, Object> getAdditionalProperties() {
			return this.additionalProperties;
		}

		public void setAdditionalProperty(String name, Object value) {
			this.additionalProperties.put(name, value);
		}

		@Override
		public String toString() {
			return "Team [teamIndex=" + teamIndex + ", numOfPlayers=" + numOfPlayers + ", players=" + players
					+ ", additionalProperties=" + additionalProperties + "]";
		}

	}

	public static class Points {
		private Map<String, PlayerPoints> points;

		public Map<String, PlayerPoints> getPoints() {
			return points;
		}

		public void setPoints(Map<String, PlayerPoints> points) {
			this.points = points;
		}

		@Override
		public String toString() {
			return "Points [points=" + points + "]";
		}

	}

	public static class PlayerPoints {

		private int dayPoints;
		private int dayBlocks;
		private int points;
		private int blocks;
		private Boolean dayPlayer;

		public int getDayPoints() {
			return dayPoints;
		}

		public void setDayPoints(int dayPoints) {
			this.dayPoints = dayPoints;
		}

		public int getDayBlocks() {
			return dayBlocks;
		}

		public void setDayBlocks(int dayBlocks) {
			this.dayBlocks = dayBlocks;
		}

		public int getPoints() {
			return points;
		}

		public void setPoints(int points) {
			this.points = points;
		}

		public int getBlocks() {
			return blocks;
		}

		public void setBlocks(int blocks) {
			this.blocks = blocks;
		}

		public Boolean getDayPlayer() {
			return dayPlayer;
		}

		public void setDayPlayer(Boolean dayPlayer) {
			this.dayPlayer = dayPlayer;
		}

		@Override
		public String toString() {
			return "PlayerPoints [dayPoints=" + dayPoints + ", dayBlocks=" + dayBlocks + ", points=" + points
					+ ", blocks=" + blocks + ", dayPlayer=" + dayPlayer + "]";
		}

	}
}
