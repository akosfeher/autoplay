package hu.itbt.autoplay.model;

import java.awt.Point;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import hu.itbt.autoplay.ApplicationConfig;

public class AutoPlayer extends Thread {

	protected static String CEGID = ApplicationConfig.getInstance().getProperty("CEGID");

	static {
		if (CEGID == null || CEGID.isEmpty()) {
			CEGID = "softic";
		}
	}

	protected boolean finished = false;

	public transient final static Map<String, List<Point>> formsMap;
//	public transient final static Map<String, List<Point>> formsMapUp;
//

	static {
		formsMap = new HashMap<>();
//		formsMapUp = new HashMap<>();
		Point[] formA = new Point[] { new Point(0, -2), new Point(0, -1), new Point(0, 0), new Point(0, 1),
				new Point(0, 2), new Point(0, 3), new Point(-1, -1), new Point(-1, 0), new Point(-1, 1),
				new Point(-1, 2), new Point(-1, 3), new Point(1, -1), new Point(1, 0), new Point(1, 1), new Point(1, 2),
				new Point(1, 3), new Point(-2, 0), new Point(-2, 1), new Point(-2, 2), new Point(-3, 1),
				new Point(2, 0), new Point(2, 1), new Point(2, 2), new Point(3, 1) };
		formsMap.put("A", Arrays.asList(formA));
//		formsMapUp.put("A", updown(Arrays.asList(formA)));
		Point[] formB = new Point[] { new Point(0, 4), new Point(0, 3), new Point(0, 2), new Point(0, 1),
				new Point(0, 0), new Point(0, -1), new Point(0, -2), new Point(0, -3), new Point(-1, 2),
				new Point(-1, 1), new Point(-1, 0), new Point(-1, -1), new Point(1, 2), new Point(1, 1),
				new Point(1, 0), new Point(1, -1), new Point(-2, 3), new Point(-2, 1), new Point(-2, 0),
				new Point(-2, -2), new Point(2, 3), new Point(2, 1), new Point(2, 0), new Point(2, -2) };
		formsMap.put("B", Arrays.asList(formB));
//		formsMapUp.put("B", Arrays.asList(formB));
		Point[] formC = new Point[] { new Point(0, -3), new Point(0, -2), new Point(0, -1), new Point(0, 0),
				new Point(0, 1), new Point(0, 2), new Point(0, 3), new Point(0, 4), new Point(-1, -2), new Point(-1, 0),
				new Point(-1, 2), new Point(-1, 4), new Point(1, -2), new Point(1, 0), new Point(1, 2), new Point(1, 4),
				new Point(-2, 0), new Point(-2, 2), new Point(-2, 4), new Point(-3, 2), new Point(2, 0),
				new Point(2, 2), new Point(2, 4), new Point(3, 2) };
		formsMap.put("C", Arrays.asList(formC));
//		formsMapUp.put("C", updown(Arrays.asList(formC)));
		Point[] formD = new Point[] { new Point(-1, 2), new Point(0, 2), new Point(1, 2), new Point(2, 2),
				new Point(-1, 1), new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(-1, 0), new Point(0, 0),
				new Point(1, 0), new Point(2, 0), new Point(-1, -1), new Point(0, -1), new Point(1, -1),
				new Point(2, -1), new Point(-3, 4), new Point(-2, 3), new Point(4, 4), new Point(3, 3),
				new Point(-3, -3), new Point(-2, -2), new Point(3, -2), new Point(4, -3) };
		formsMap.put("D", Arrays.asList(formD));
//		formsMapUp.put("D", Arrays.asList(formD));
		Point[] formE = new Point[] { new Point(0, 4), new Point(0, 3), new Point(0, 2), new Point(0, 1),
				new Point(0, 0), new Point(0, -1), new Point(0, -2), new Point(0, -3), new Point(-1, 3),
				new Point(-1, 2), new Point(-1, -1), new Point(-1, -2), new Point(1, 3), new Point(1, 2),
				new Point(1, -1), new Point(1, -2), new Point(-2, 2), new Point(-3, 2), new Point(-2, -1),
				new Point(-3, -1), new Point(2, 2), new Point(3, 2), new Point(2, -1), new Point(3, -1) };
		formsMap.put("E", Arrays.asList(formE));
//		formsMapUp.put("E", Arrays.asList(formE));
		Point[] formF = new Point[] { new Point(-3, -3), new Point(-2, -2), new Point(-1, -1), new Point(0, 0),
				new Point(1, 1), new Point(2, 2), new Point(3, 3), new Point(4, 4), new Point(-3, 4), new Point(-2, 3),
				new Point(-1, 2), new Point(0, 1), new Point(1, 0), new Point(2, -1), new Point(3, -2),
				new Point(4, -3), new Point(0, 2), new Point(0, 3), new Point(0, 4), new Point(1, 2), new Point(1, 3),
				new Point(1, 4), new Point(-1, 3), new Point(2, 3) };
		formsMap.put("F", Arrays.asList(formF));
//		formsMapUp.put("F", updown(Arrays.asList(formF)));
	}

	// private transient static int[] updown = new int[] { 5, 4, 3, 2, 1, 0, -1, -2,
	// -3, -4 };

	// protected int[][] matrix = new int[21][21];
	protected transient Map<Integer, Integer> teamNumOfLocations;
	protected Map<Integer, String> teamForm;

	private transient Logger logger;
	// private transient Socket socket;
	private User user;
	private String tp;
	private int teamIndex;
	private int playerIndex;
	private long gameId;
	private String formLetter;
	private RobotBoard robotBoard;

	private transient Client client;

	protected static String g4tApiUrl = ApplicationConfig.getInstance().getProperty("G4T_API_URL");

	static {
		if (g4tApiUrl == null || g4tApiUrl.isEmpty()) {
			g4tApiUrl = "http://localhost:8080/g4t/rest";
		}
	}

	public AutoPlayer(String playerName, User u, RobotBoard rb) throws InterruptedException {
		super();
		this.playerName = playerName;
		this.user = u;
		this.robotBoard = rb;

		logger = Logger.getLogger("AutoLogger-" + playerName);

		client = ClientBuilder.newClient();

		WebTarget wtactgame = client.target(g4tApiUrl + "/users/" + CEGID + "/activegame/" + user.getId());

		String retu = null;
		try {
			retu = wtactgame.request(MediaType.APPLICATION_JSON).get(String.class);
		} catch (Exception e) {
			e.printStackTrace();
			interrupt();
			logger.info("activegame rest service interrupted");
			return;
		}
		System.out.println("activegame for: " + user.getName() + "\nretu\n" + retu);

		Gson gson = new Gson();

		JsonParser parser = new JsonParser();

		JsonObject obj = parser.parse(retu).getAsJsonObject();

		String ps = obj.get("points").getAsJsonObject().toString();

		Map<String, ActiveGame.PlayerPoints> pp = gson.fromJson(ps,
				new TypeToken<HashMap<String, ActiveGame.PlayerPoints>>() {
				}.getType());

//		if (robotpoints > 0) {
//			updateRobotPoints(pp, robotpoints);
//		}
		ActiveGame ag = gson.fromJson(retu, ActiveGame.class);

		ActiveGame.Points points = new ActiveGame.Points();
		points.setPoints(pp);

		ag.setPoints(points);

		setTp(ag.getPlayer().getTeamIndex() + "-" + ag.getPlayer().getPlayerIndex());

		setTeamIndex(ag.getPlayer().getTeamIndex());

		setPlayerIndex(ag.getPlayer().getPlayerIndex());

		setGameId(ag.getGameId());

		setFormLetter(ag.getPlayer().getForm());

//		fillMatrix(ag.getPoziciok());

		logger.info(ag.toString());
	}

	@Override
	public String toString() {
		return "AutoPlayer [finished=" + finished + ", teamNumOfLocations=" + teamNumOfLocations + ", teamForm="
				+ teamForm + ", logger=" + logger + ", user=" + user + ", tp=" + tp + ", teamIndex=" + teamIndex
				+ ", playerIndex=" + playerIndex + ", gameId=" + gameId + ", formLetter=" + formLetter + ", client="
				+ client + ", playerName=" + playerName + "]";
	}

	public String getFormLetter() {
		return formLetter;
	}

	public void setFormLetter(String formLetter) {
		this.formLetter = formLetter;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public Map<Integer, String> getTeamForm() {
		return teamForm;
	}

	public void setTeamForm(Map<Integer, String> teamForm) {
		this.teamForm = teamForm;
	}

	public int getTeamIndex() {
		return teamIndex;
	}

	public void setTeamIndex(int teamIndex) {
		this.teamIndex = teamIndex;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public String getTp() {
		return tp;
	}

	public void setTp(String tp) {
		this.tp = tp;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	protected String playerName;

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	@Override
	public void run() {
		logger.info("Autoplayer " + this.playerName + " is running");

		Gson gson = new Gson();

		try {
//			createSocket();

			// sleep(30000);

			for (int i = 0; i < 1000; i++) {
				if (isInterrupted()) {
					logger.info("during move loop interrupted.");
					// socket.disconnect();
					break;
				}
				if (robotBoard.isFinished()) {
					logger.info("during move robotBoard isFinished.");
					// socket.disconnect();
					break;
				}
				if (robotBoard.getBlocks(tp) < 1 && robotBoard.getPoints(tp) < 1) {
					logger.info("during move no more points blocks.");
					break;
				}
				int delay = robotBoard.getDelay();
				sleep((long) (delay + Math.random() * delay / 3));
				int x = rand(21);
				int y = rand(21);
				Point point = selectPlay();
				if (point != null) {
					move(point.x, point.y);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			logger.info("AutoPlayer finally");
		}

	}

	private Point selectPlay() {
		// logger.info("dddddddddddddddddays end in secs: " +
		// robotBoard.getSecondsToDayend());
		int points = robotBoard.getPoints(tp);
		int blocks = robotBoard.getBlocks(tp);
		if (blocks + points < 1) {
			return null;
		}
		if (points == 0 && blocks > 0) {
			if (robotBoard.getSecondsToDayend() < 15) {
				// 1. repare own shape if necesary
				boolean findBlock = true;
				boolean hasBlock = true;
				Point block = findPointOrBlockPosi(findBlock, hasBlock);
				if (block != null) {
					return block;
				}
				// 2. make harm to others
				for (int loo = 0; loo < 100; loo++) {
					int xtr = (int) (Math.random() * 20);
					int ytr = (int) (Math.random() * 20);
					if (robotBoard.matrix[xtr][ytr] == getTeamIndex() || robotBoard.matrix[xtr][ytr] == 0) {
						continue;
					}
					return new Point(xtr, ytr);
				}
			}
			return null;
		}
// If we have no point jus a block, sometimes we harm the enemy
		if (Math.random() < 0.2 && points == 0 && blocks > 0) {
			for (int loo = 0; loo < 100; loo++) {
				int xtr = (int) (Math.random() * 20);
				int ytr = (int) (Math.random() * 20);
				if (robotBoard.matrix[xtr][ytr] == getTeamIndex() || robotBoard.matrix[xtr][ytr] == 0) {
					continue;
				}
				return new Point(xtr, ytr);
			}
		}

		boolean findBlock = false;
		boolean hasBlock = blocks > 0;
		return findPointOrBlockPosi(findBlock, hasBlock);
	}

	private Point findPointOrBlockPosi(boolean findBlock, boolean hasBlock) {
		List<Point> form = formsMap.get(getFormLetter());
		int minimum = 200;
		List<Point> minimums = new ArrayList<>();
		for (int xtry = 0; xtry < 21; xtry++) {
			for (int ytry = 0; ytry < 21; ytry++) {
				if (robotBoard.matrix[xtry][ytry] == getTeamIndex()) {
					continue;
				}
				if (findBlock && robotBoard.matrix[xtry][ytry] == 0) {
					continue;
				}
				// Position the forms all places on top of xtry,ytry
				for (Point formPosi : form) {
					int value = computeFinishScore(xtry, ytry, formPosi.x, formPosi.y, form, hasBlock);
					if (value < minimum) {
						minimum = value;
						minimums = new ArrayList<>();
						minimums.add(new Point(xtry, ytry));
					} else if (value == minimum) {
						minimums.add(new Point(xtry, ytry));
					}
				}
			}
		}
		if (minimums.size() == 0) {
			return null;
		}
		Random rand = new Random();
		int minInd = rand.nextInt(minimums.size());

		return minimums.get(minInd);
	}

	// xtry,ytry board position, formPosiX formPosiY relative coordinates where the
	// form positioned on xtry, ytry
	private synchronized int computeFinishScore(int xtry, int ytry, int formPosiX, int formPosiY, List<Point> form,
			boolean hasBlock) {
		int coast = 0;
		for (Point posiToCheck : form) {
			int actX = xtry + posiToCheck.x - formPosiX;
			int actY = ytry + posiToCheck.y - formPosiY;
			// if form does not fit the board it is dropped
			if (actX < 0 || actX > 20 || actY < 0 || actY > 20) {
				return 1000;
			}
			if (robotBoard.matrix[actX][actY] == 0) {
				coast++;
			} else if (robotBoard.matrix[actX][actY] != getTeamIndex()) {
				if (hasBlock) {
					hasBlock = false;
					coast++;
				} else {
					coast += 2;
				}
			} else {
				// logger.info("Own location teamIdx: " + getTeamIndex() + " x,y: " + actX + ",
				// " + actY);
			}
		}
		return coast;
	}

	private int rand(int range) {
		int r = (int) (Math.random() * range);
		return r;
	}

	public void move(int x, int y) throws JSONException, URISyntaxException {
		robotBoard.move(tp, x, y);
	}

	public static class OnDrawData {
		private float gameId;
		private boolean isBlock;
		private int x;
		private int y;
		private String tp;
		private String tenant;

		// Getter Methods

		public float getGameId() {
			return gameId;
		}

		public boolean getIsBlock() {
			return isBlock;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public String getTp() {
			return tp;
		}

		public int getTeamIndex() {
			return Integer.parseInt(tp.substring(0, 1));
		}

		public String getTenant() {
			return tenant;
		}

		// Setter Methods

		public void setGameId(float gameId) {
			this.gameId = gameId;
		}

		public void setIsBlock(boolean isBlock) {
			this.isBlock = isBlock;
		}

		public void setX(int x) {
			this.x = x;
		}

		public void setY(int y) {
			this.y = y;
		}

		public void setTp(String tp) {
			this.tp = tp;
		}

		public void setTenant(String tenant) {
			this.tenant = tenant;
		}
	}
}
