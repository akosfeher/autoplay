package hu.itbt.autoplay.model;

import java.awt.Point;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import hu.itbt.autoplay.ApplicationConfig;
import hu.itbt.autoplay.model.ActiveGame.PlayerPoints;
import hu.itbt.autoplay.model.ActiveGame.Poziciok;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class RobotBoard {

	protected static String CEGID = ApplicationConfig.getInstance().getProperty("CEGID");

	static {
		if (CEGID == null || CEGID.isEmpty()) {
			CEGID = "softic";
		}
	}

	public transient final static Map<String, List<Point>> formsMap;
	public transient final static Map<String, List<Point>> formsMapUp;

	protected boolean finished = false;

	static {
		formsMap = new HashMap<>();
		formsMapUp = new HashMap<>();
		Point[] formA = new Point[] { new Point(0, -2), new Point(0, -1), new Point(0, 0), new Point(0, 1),
				new Point(0, 2), new Point(0, 3), new Point(-1, -1), new Point(-1, 0), new Point(-1, 1),
				new Point(-1, 2), new Point(-1, 3), new Point(1, -1), new Point(1, 0), new Point(1, 1), new Point(1, 2),
				new Point(1, 3), new Point(-2, 0), new Point(-2, 1), new Point(-2, 2), new Point(-3, 1),
				new Point(2, 0), new Point(2, 1), new Point(2, 2), new Point(3, 1) };
		formsMap.put("A", Arrays.asList(formA));
		formsMapUp.put("A", updown(Arrays.asList(formA)));
		Point[] formB = new Point[] { new Point(0, 4), new Point(0, 3), new Point(0, 2), new Point(0, 1),
				new Point(0, 0), new Point(0, -1), new Point(0, -2), new Point(0, -3), new Point(-1, 2),
				new Point(-1, 1), new Point(-1, 0), new Point(-1, -1), new Point(1, 2), new Point(1, 1),
				new Point(1, 0), new Point(1, -1), new Point(-2, 3), new Point(-2, 1), new Point(-2, 0),
				new Point(-2, -2), new Point(2, 3), new Point(2, 1), new Point(2, 0), new Point(2, -2) };
		formsMap.put("B", Arrays.asList(formB));
		formsMapUp.put("B", Arrays.asList(formB));
		Point[] formC = new Point[] { new Point(0, -3), new Point(0, -2), new Point(0, -1), new Point(0, 0),
				new Point(0, 1), new Point(0, 2), new Point(0, 3), new Point(0, 4), new Point(-1, -2), new Point(-1, 0),
				new Point(-1, 2), new Point(-1, 4), new Point(1, -2), new Point(1, 0), new Point(1, 2), new Point(1, 4),
				new Point(-2, 0), new Point(-2, 2), new Point(-2, 4), new Point(-3, 2), new Point(2, 0),
				new Point(2, 2), new Point(2, 4), new Point(3, 2) };
		formsMap.put("C", Arrays.asList(formC));
		formsMapUp.put("C", updown(Arrays.asList(formC)));
		Point[] formD = new Point[] { new Point(-1, 2), new Point(0, 2), new Point(1, 2), new Point(2, 2),
				new Point(-1, 1), new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(-1, 0), new Point(0, 0),
				new Point(1, 0), new Point(2, 0), new Point(-1, -1), new Point(0, -1), new Point(1, -1),
				new Point(2, -1), new Point(-3, 4), new Point(-2, 3), new Point(4, 4), new Point(3, 3),
				new Point(-3, -3), new Point(-2, -2), new Point(3, -2), new Point(4, -3) };
		formsMap.put("D", Arrays.asList(formD));
		formsMapUp.put("D", Arrays.asList(formD));
		Point[] formE = new Point[] { new Point(0, 4), new Point(0, 3), new Point(0, 2), new Point(0, 1),
				new Point(0, 0), new Point(0, -1), new Point(0, -2), new Point(0, -3), new Point(-1, 3),
				new Point(-1, 2), new Point(-1, -1), new Point(-1, -2), new Point(1, 3), new Point(1, 2),
				new Point(1, -1), new Point(1, -2), new Point(-2, 2), new Point(-3, 2), new Point(-2, -1),
				new Point(-3, -1), new Point(2, 2), new Point(3, 2), new Point(2, -1), new Point(3, -1) };
		formsMap.put("E", Arrays.asList(formE));
		formsMapUp.put("E", Arrays.asList(formE));
		// test form
//    	Point[] formE = new  Point[] {
//    			new Point(-1,4), new Point(0,4), new Point(1,4), new Point(2,4),new Point(3,4),new Point(4,4),new Point(2,3), new Point(3,3),
//    			new Point(-3,2), new Point(-2,2), new Point(-1,2),new Point(0,2), new Point(1,2), new Point(2,2), 
//    			new Point(-2,1), new Point(1,1), new Point(-3,0),new Point(-2,-1), new Point(2,-1),
//    			new Point(-3,-3), new Point(-2,-3), new Point(-1,-3), new Point(0,-3), new Point(1,-3)};
//    	formsMap.put("E", Arrays.asList(formE));
		Point[] formF = new Point[] { new Point(-3, -3), new Point(-2, -2), new Point(-1, -1), new Point(0, 0),
				new Point(1, 1), new Point(2, 2), new Point(3, 3), new Point(4, 4), new Point(-3, 4), new Point(-2, 3),
				new Point(-1, 2), new Point(0, 1), new Point(1, 0), new Point(2, -1), new Point(3, -2),
				new Point(4, -3), new Point(0, 2), new Point(0, 3), new Point(0, 4), new Point(1, 2), new Point(1, 3),
				new Point(1, 4), new Point(-1, 3), new Point(2, 3) };
		formsMap.put("F", Arrays.asList(formF));
		formsMapUp.put("F", updown(Arrays.asList(formF)));
	}

	private transient static int[] updown = new int[] { 5, 4, 3, 2, 1, 0, -1, -2, -3, -4 };

	protected int[][] matrix = new int[25][25];
	protected transient Map<Integer, Integer> teamNumOfLocations;
	protected Map<Integer, String> teamForm;

	private transient Logger logger = Logger.getLogger(RobotBoard.class.getName());

	private transient Socket socket;
	private ActiveGame activeGame;
	private long gameId;
	private String cegId;
	private int robotpoints;

	private transient Client client;

	protected static String g4tApiUrl = ApplicationConfig.getInstance().getProperty("G4T_API_URL");
	protected static int delay;

	static {
		try {
			delay = ApplicationConfig.getInstance().getProperty("STEP_DELAY") != null
					? Integer.parseInt(ApplicationConfig.getInstance().getProperty("STEP_DELAY"))
					: 6000;
		} catch (NumberFormatException e) {
			delay = 6000;
		}
	}

	static {
		if (g4tApiUrl == null || g4tApiUrl.isEmpty()) {
			g4tApiUrl = "http://localhost:8080/g4t/rest";
		}
	}

	protected static String socketUrl = ApplicationConfig.getInstance().getProperty("SOCKET_URL");

	static {
		if (socketUrl == null || socketUrl.isEmpty()) {
			socketUrl = "http://localhost:8082";
		}
	}

	public RobotBoard(String cegId, long activeGameId) {
		super();

		this.cegId = cegId;
		this.gameId = activeGameId;

		this.robotpoints = robotpoints;

		client = ClientBuilder.newClient();

		WebTarget wtactgame = client.target(g4tApiUrl + "/admin/" + CEGID + "/" + activeGameId + "/game");

		String retu = null;
		try {
			retu = wtactgame.request(MediaType.APPLICATION_JSON).get(String.class);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("game rest service exception");
			return;
		}

		Gson gson = new Gson();

		JsonParser parser = new JsonParser();

		JsonObject obj = parser.parse(retu).getAsJsonObject();

		String ps = obj.get("points").getAsJsonObject().toString();

		Map<String, ActiveGame.PlayerPoints> pp = gson.fromJson(ps,
				new TypeToken<HashMap<String, ActiveGame.PlayerPoints>>() {
				}.getType());

//		// if robotpoints are set, all the robots are getting the points at restart
//		if (robotpoints > 0) {
//			updateRobotPoints(pp, robotpoints);
//		}

		activeGame = gson.fromJson(retu, ActiveGame.class);

		ActiveGame.Points points = new ActiveGame.Points();
		points.setPoints(pp);

		activeGame.setPoints(points);

		fillMatrix(activeGame.getPoziciok());

		logger.info("RobotBoard game: ------------------------------------------------------------");
		logger.info(activeGame.toString());
	}

	public int getDelay() {
		return delay;
	}

//	private void updateRobotPoints(Map<String, PlayerPoints> pp, int robotpoints2) {
//		for (PlayerPoints plpo : pp.values()) {
//			plpo.setDayPoints(robotpoints);
//			plpo.setPoints(robotpoints);
//			int blocks = Math.random() < 0.6 ? 1 : 0;
//			plpo.setDayBlocks(blocks);
//			plpo.setBlocks(blocks);
//		}
//	}

	@Override
	public String toString() {
		return "RobotBoard [finished=" + finished + ", matrix=" + Arrays.toString(matrix) + ", teamForm=" + teamForm
				+ ", activeGame=" + activeGame + ", gameId=" + gameId + "]";
	}

	public long getSecondsToDayend() {
		return activeGame.secondsToDayend();
	}

	private void fillMatrix(List<Poziciok> poziciok) {
		for (int x = 0; x < 21; x++) {
			for (int y = 0; y < 21; y++) {
				matrix[x][y] = 0;
			}
		}
		for (Poziciok pozi : poziciok) {
			int teamIndex = Integer.parseInt(pozi.getTp().substring(0, 1));
			matrix[pozi.getX()][pozi.getY()] = teamIndex;
		}

	}

	private static List<Point> updown(List<Point> asList) {
		if (updown == null) {
			updown = new int[] { 5, 4, 3, 2, 1, 0, -1, -2, -3, -4 };
		}
		List<Point> ret = new ArrayList<>();
		for (Point p : asList) {
			Point np = new Point(p.x, updown[4 + p.y]);
			ret.add(np);
		}
		return ret;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public Map<Integer, String> getTeamForm() {
		return teamForm;
	}

	public void setTeamForm(Map<Integer, String> teamForm) {
		this.teamForm = teamForm;
	}

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public void initSocket() {
		logger.info("RobotBoard socket is inited");

		Gson gson = new Gson();

		try {
			createSocket();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (socket != null) {
				logger.info("run finally socket.disconnect()");
				socket.disconnect();
			}
		}

	}

	private void createSocket() throws URISyntaxException {
		Gson gson = new Gson();
		socket = IO.socket(socketUrl);

		socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

			@Override
			public void call(Object... args) {
				JSONObject obj = new JSONObject();
				try {
					obj.put("gameId", getGameId());
					obj.put("tenant", CEGID);
					obj.put("name", "RobotBoard");
					obj.put("team", 0);
					obj.put("playerIndex", 0);

					socket.emit("join", obj);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String id = socket.id();

				logger.info("Socket connect callback -------------- ,,,,,,,,,,,,,   socket id: " + id + "  connected: "
						+ socket.connected());

				// socket.disconnect();
			}

		}).on("status", new Emitter.Listener() {

			@Override
			public void call(Object... args) {
				logger.info("-----------------  onStatus args[0]: " + args[0]);
				try {
					String status = (String) ((JSONObject) args[0]).get("status");
					if ("WIN".equals(status)) {
						logger.info("WIN status, thread interrupted.");
						socket.disconnect();
						setFinished(true);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}).on("draw", new Emitter.Listener() {

			@Override
			public void call(Object... args) {
				logger.info("-----------------  onDraw: " + args[0]);
				OnDrawData onDraw = gson.fromJson(args[0].toString(), OnDrawData.class);

				int teamIdx = onDraw.getTeamIndex();
				matrix[onDraw.getX()][onDraw.getY()] = teamIdx;
				updatePoints(onDraw.isBlock, onDraw.getTp());
			}

		}).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

			@Override
			public void call(Object... args) {
				logger.info("-----------------  onDisconnect: " + args);
				socket.disconnect();
			}

		});

		socket.connect();
		logger.info("After socket.connect() connected: " + socket.connected());

		logger.info(socket.id());

	}

	@Override
	protected void finalize() {
		this.socket.disconnect();
	}

	private synchronized void updatePoints(boolean isBlock, String tp) {
		PlayerPoints playerPoints = this.activeGame.getPoints().getPoints().get(tp);
		if (playerPoints == null) {
			return;
		}
		int blocks = playerPoints.getBlocks();
		int points = playerPoints.getPoints();
		if (isBlock) {
			if (blocks > 0) {
				playerPoints.setBlocks(blocks - 1);
			} else {
				playerPoints.setPoints(((points > 2) ? points - 2 : 0));
			}
		} else {
			playerPoints.setPoints(((points > 0) ? points - 1 : 0));
		}
	}

	public int getPoints(String tp) {
		PlayerPoints playerPoints = this.activeGame.getPoints().getPoints().get(tp);
		if (playerPoints == null) {
			return 0;
		}
		int points = playerPoints == null ? 0 : playerPoints.getPoints();
		return points;
	}

	public int getBlocks(String tp) {
		PlayerPoints playerPoints = this.activeGame.getPoints().getPoints().get(tp);
		if (playerPoints == null) {
			return 0;
		}
		int blocks = playerPoints == null ? 0 : playerPoints.getBlocks();
		return blocks;
	}

	public void move(String tp, int x, int y) throws JSONException, URISyntaxException {
		boolean block = matrix[x][y] != 0;
		logger.info("::::::::::::::  " + (block ? "blockMove" : "move") + " x,y: " + x + "," + y + " tp: " + tp
				+ "  gameId: " + getGameId());
		JSONObject obj = new JSONObject();
		obj.put("gameId", getGameId());
		obj.put("tenant", CEGID);
		obj.put("isBlock", false);
		obj.put("x", x);
		obj.put("y", y);
		obj.put("tp", tp);
		if (socket == null) {
			createSocket();
			logger.info("Move new socket");
		}
		Emitter emitter = socket.emit((block ? "blockMove" : "move"), obj);
		// emitter.emit((block ? "blockMove" : "move"), obj);
		logger.info("Move emitter: " + emitter.listeners("connect"));
		logger.info("Move emitter: " + emitter.listeners("status"));
		logger.info("Move emitter: " + emitter.listeners("draw"));

	}

	public static class OnDrawData {
		private float gameId;
		private boolean isBlock;
		private int x;
		private int y;
		private String tp;
		private String tenant;

		// Getter Methods

		public float getGameId() {
			return gameId;
		}

		public boolean getIsBlock() {
			return isBlock;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public String getTp() {
			return tp;
		}

		public int getTeamIndex() {
			return Integer.parseInt(tp.substring(0, 1));
		}

		public String getTenant() {
			return tenant;
		}

		// Setter Methods

		public void setGameId(float gameId) {
			this.gameId = gameId;
		}

		public void setIsBlock(boolean isBlock) {
			this.isBlock = isBlock;
		}

		public void setX(int x) {
			this.x = x;
		}

		public void setY(int y) {
			this.y = y;
		}

		public void setTp(String tp) {
			this.tp = tp;
		}

		public void setTenant(String tenant) {
			this.tenant = tenant;
		}
	}
}
