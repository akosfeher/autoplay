package hu.itbt.autoplay.pages;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import hu.itbt.autoplay.utils.JWTRoleNeeded;

@Path("/page")
@Produces(MediaType.TEXT_HTML)
public class IndexPage {

	@GET
	@Path("/index")
	@JWTRoleNeeded({ "NONE" })
	public Response view() {
		StringBuilder sb = new StringBuilder();

		sb.append("<!DOCTYPE html>");
		sb.append("<html lang=\"en\">");
		sb.append("<head>");
		sb.append("    <meta charset=\"UTF-8\">");
		sb.append("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
		sb.append("    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">");
		sb.append("    <title></title>");
		sb.append("</head>");
		sb.append("<body>");

		sb.append("Hello from server!");

		sb.append("</body>");
		sb.append("</html>");

		return Response.status(200).entity(sb.toString()).build();
	}
}
