package hu.itbt.autoplay.rest;

import java.util.Map;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import hu.itbt.autoplay.model.AutoPlayer;
import hu.itbt.autoplay.model.RobotBoard;
import hu.itbt.autoplay.service.RobotRepo;

@Path("/robots")
public class RobotRest {

	private static Logger logger = Logger.getLogger(RobotRest.class.getName());

	protected static RobotRepo robotRepo;

	protected static RobotRepo getRobotRepo() {
		if (robotRepo == null) {
			robotRepo = new RobotRepo();
		}
		return robotRepo;
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/robotboard/{activeGameId}/started")
	// @JWTRoleNeeded({"ADMIN", "CEGADMIN"})
	// @EventLog(value = "USER_ALL")
	public Response createRobotBoardForGame(@PathParam("cegId") String cegId,
			@PathParam("activeGameId") long activeGameId, @HeaderParam("x-access-token") String token, String payload) {

		RobotBoard rb = getRobotRepo().createRobotBoard(cegId, activeGameId);

		getRobotRepo().createActiveGameRobots(cegId, activeGameId, rb);

		return Response.status(200).entity("OK").build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/robotboards/ended")
	// @JWTRoleNeeded({"ADMIN", "CEGADMIN"})
	// @EventLog(value = "USER_ALL")
	public Response destroyAllRobotStuff(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			String payload) {

		getRobotRepo().removeRobots(cegId);

		getRobotRepo().removeRobotBoards(cegId);

		return Response.status(200).entity("OK").build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/robotboard/{activeGameId}/ended")
	// @JWTRoleNeeded({"ADMIN", "CEGADMIN"})
	// @EventLog(value = "USER_ALL")
	public Response destroyGameRobotStuff(@PathParam("cegId") String cegId,
			@PathParam("activeGameId") long activeGameId, @HeaderParam("x-access-token") String token, String payload) {

		getRobotRepo().removeActivegameRobots(cegId, activeGameId);

		getRobotRepo().removeGameRobotBoards(cegId, activeGameId);

		return Response.status(200).entity("OK").build();
	}

//	@GET
//	// @Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/{cegId}/{activeGameId}/ended")
//	// @JWTRoleNeeded({"ADMIN", "CEGADMIN"})
//	// @EventLog(value = "USER_ALL")
//	public Response stopRobotsForActiveGame(@PathParam("cegId") String cegId,
//			@PathParam("activeGameId") long activeGameId, @HeaderParam("x-access-token") String token, String payload) {
//
//		getRobotRepo().activeGameEnded(cegId, activeGameId);
//
//		return Response.status(200).entity("OK").build();
//	}

//	@GET
//	// @Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/{cegId}/{activeGameId}/started")
//	// @JWTRoleNeeded({"ADMIN", "CEGADMIN"})
//	// @EventLog(value = "USER_ALL")
//	public Response createRobotsForActiveGame(@PathParam("cegId") String cegId,
//			@PathParam("activeGameId") long activeGameId, @HeaderParam("x-access-token") String token, String payload) {
//
//		getRobotRepo().createActiveGameRobots(cegId, activeGameId);
//
//		return Response.status(200).entity("OK").build();
//	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/robotmap")
	// @JWTRoleNeeded({"ADMIN", "CEGADMIN"})
	// @EventLog(value = "USER_ALL")
	public Response getRobotmap(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			String payload) {

		Map<Long, Map<Long, AutoPlayer>> map = getRobotRepo().getRobotMap();

		logger.info(map.toString());
		ExclusionStrategy strategy = new ExclusionStrategy() {
			@Override
			public boolean shouldSkipClass(Class<?> clazz) {
				return false;
			}

			@Override
			public boolean shouldSkipField(FieldAttributes field) {
				return field.getName().startsWith("socket");
			}
		};

		Gson gson = new GsonBuilder().addSerializationExclusionStrategy(strategy).create();
		String ret = gson.toJson(map);

		return Response.status(200).entity(ret).build();
	}

}