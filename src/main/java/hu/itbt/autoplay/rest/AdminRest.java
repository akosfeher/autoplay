package hu.itbt.autoplay.rest;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.json.JSONException;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import hu.itbt.autoplay.StartupListener;
import hu.itbt.autoplay.model.AuthClaims;
import hu.itbt.autoplay.model.User;
import hu.itbt.autoplay.service.Identifiable;
import hu.itbt.autoplay.service.SendEmail;
import hu.itbt.autoplay.service.UserRepo;
import hu.itbt.autoplay.utils.EventLog;
import hu.itbt.autoplay.utils.JWTRoleNeeded;
import hu.itbt.autoplay.utils.Utils;

@Path("/admin")
@ApplicationScoped
//@JWTRoleNeeded({"LOVACSKA", "CICA"})
public class AdminRest {

	private static Logger logger = Logger.getLogger(AdminRest.class.getName());

	public static final boolean NO_LEFTOUT_COLUMN = true;

	@PostConstruct
	public void init() {
		System.out.println("UserRest created");
	}

	public AdminRest() {
		super();
	}

	// @Inject
	protected static UserRepo userRepo;

	protected static UserRepo getUserRepo() {
		if (userRepo == null) {
			userRepo = new UserRepo();
		}
		return userRepo;
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces({ "image/png", "image/jsp", "image/gif" })
	@Path("/{cegId}/{userId}/portrait")
	// @JWTRoleNeeded({"ADMIN", "USER", "CEGADMIN"})
	// @EventLog(value="USER_BYID")
	public Response getUserPortrait(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			@PathParam("userId") long userId, @QueryParam("size") int size) throws IOException {

		Identifiable ifa = new Identifiable(userId);

		byte[] portrait = getUserRepo().getPortrait(cegId, userId);

		if (size > 5) {
			byte[] smallportrait = resizeImageAsJPG(portrait, size);

			ResponseBuilder response = Response.ok(smallportrait);
			return response.build();
		} else {
			ResponseBuilder response = Response.ok(portrait);
			return response.build();

		}
		// response.header("Content-Disposition", "attachment; filename=portrait.gif");

	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	// @Produces({ "image/png", "image/jsp", "image/gif" })
	@Path("/email/{to}/{subject}/{content}")
	// @JWTRoleNeeded({"ADMIN", "USER", "CEGADMIN"})
	// @EventLog(value="USER_BYID")
	public Response sendEmail(@PathParam("to") String to, @PathParam("subject") String subject,
			@PathParam("content") String content, @HeaderParam("x-access-token") String token) {

		SendEmail.sendSimpleEmail(true, to, subject, "<html><body><h3>" + content + "</h3></body></html>", content,
				false);

		ResponseBuilder response = Response.ok("ok");
		// response.header("Content-Disposition", "attachment; filename=portrait.gif");
		return response.build();
	}

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	// @Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/{userId}/portrait")
	// @JWTRoleNeeded({"ADMIN", "USER", "CEGADMIN"})
	// @EventLog(value="USER_BYID")
	public Response putUserUserPortrait(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			@PathParam("userId") long userId, MultipartFormDataInput input) {

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("portrait");

		InputPart inputPart = inputParts.get(0);

		try {

			// convert the uploaded file to inputstream
			InputStream inputStream = inputPart.getBody(InputStream.class, null);

			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[1024];
			while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}

			buffer.flush();
			byte[] bytes = buffer.toByteArray();

			String ret = getUserRepo().updatePortrait(cegId, userId, bytes);

			System.out.println("Done");

			return Response.status(200).entity(ret).build();

		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(500).entity(e.getLocalizedMessage()).build();
		}

	}

	/**
	 * This method takes in an image as a byte array (currently supports GIF, JPG,
	 * PNG and possibly other formats) and resizes it to have a width no greater
	 * than the pMaxWidth parameter in pixels. It converts the image to a standard
	 * quality JPG and returns the byte array of that JPG image.
	 * 
	 * @param pImageData the image data.
	 * @param pMaxWidth  the max width in pixels, 0 means do not scale.
	 * @return the resized JPG image.
	 * @throws IOException if the iamge could not be manipulated correctly.
	 */
	public byte[] resizeImageAsJPG(byte[] pImageData, int pMaxWidth) throws IOException {
		// Create an ImageIcon from the image data
		ImageIcon imageIcon = new ImageIcon(pImageData);
		int width = imageIcon.getIconWidth();
		int height = imageIcon.getIconHeight();
		logger.info("imageIcon width: " + width + "  height: " + height);
		// If the image is larger than the max width, we need to resize it
		if (pMaxWidth > 0 && width > pMaxWidth) {
			// Determine the shrink ratio
			double ratio = (double) pMaxWidth / imageIcon.getIconWidth();
			logger.info("resize ratio: " + ratio);
			height = (int) (imageIcon.getIconHeight() * ratio);
			width = pMaxWidth;
			logger.info("imageIcon post scale width: " + width + "  height: " + height);
		}
		// Create a new empty image buffer to "draw" the resized image into
		BufferedImage bufferedResizedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		// Create a Graphics object to do the "drawing"
		Graphics2D g2d = bufferedResizedImage.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		// Draw the resized image
		g2d.drawImage(imageIcon.getImage(), 0, 0, width, height, null);
		g2d.dispose();
		// Now our buffered image is ready
		// Encode it as a JPEG

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bufferedResizedImage, "jpg", baos);
		byte[] resizedImageByteArray = baos.toByteArray();

//		ByteArrayOutputStream encoderOutputStream = new ByteArrayOutputStream();
//		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(encoderOutputStream);
//		encoder.encode(bufferedResizedImage);
//		byte[] resizedImageByteArray = encoderOutputStream.toByteArray();
		return resizedImageByteArray;
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/{userId}")
	@JWTRoleNeeded({ "ADMIN", "USER", "CEGADMIN" })
	@EventLog(value = "USER_BYID")
	public Response searchUserById(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			@PathParam("userId") long userId, String payload) {

		Identifiable ifa = new Identifiable(userId);

		String ret = getUserRepo().getById(cegId, ifa, false);
		return Response.status(200).entity(ret).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/free/players")
	// @JWTRoleNeeded({ "ADMIN", "USER", "CEGADMIN" })
	// @EventLog(value = "USER_BYID")
	public Response searchFreeUser(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			String payload) {

		Gson gson = new Gson();

		String ret = getUserRepo().getFreePlayers(cegId);

		Type listType = new TypeToken<ArrayList<User>>() {
		}.getType();
		List<User> users = gson.fromJson(ret, listType);

		StringBuilder sb = new StringBuilder();
		for (User u : users) {
			sb.append(u.getName() + "," + u.getEmail() + "\n");
		}
		return Response.status(200).entity(sb.toString()).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}")
	// @JWTRoleNeeded({"ADMIN", "CEGADMIN"})
	// @EventLog(value = "USER_ALL")
	public Response getAllUsers(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			@QueryParam("mode") String mode, String payload) {

		String ret = getUserRepo().getAll(cegId, true);

		Gson gson = new Gson();
		JsonParser parser = new JsonParser();

		List<User> users = gson.fromJson(ret, new TypeToken<List<User>>() {
		}.getType());

		for (User user : users) {
			if ("notactive".equals(mode) && user.getActive_game_id() > 0) {
				continue;
			}
			user.setRegistered(user.getHash() != null && !user.getHash().isEmpty());
			user.setHash("");
			user.setSalt("");
			if ("UNKNOWN".equals(user.getRole())) {
				user.setRole("");
			}
		}
		String ret1 = gson.toJson(users);

		return Response.status(200).entity(ret1).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/users")
	// @JWTRoleNeeded({"ADMIN", "CEGADMIN"})
	// @EventLog(value = "USER_ALL")
	public Response getCompanyUsrs(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			@QueryParam("mode") String mode, String payload) {

		String ret = getUserRepo().getAll(cegId, true);

		Gson gson = new Gson();
		JsonParser parser = new JsonParser();

		List<User> users = gson.fromJson(ret, new TypeToken<List<User>>() {
		}.getType());

		List<User> notactives = new ArrayList<>();

		for (User user : users) {
			if ("notactive".equals(mode) && user.getActive_game_id() > 0) {
				continue;
			}
			user.setRegistered(user.getHash() != null && !user.getHash().isEmpty());
			user.setHash("");
			user.setSalt("");
			if ("UNKNOWN".equals(user.getRole())) {
				user.setRole("");
			}
			notactives.add(user);
		}
		String ret1 = gson.toJson(notactives);

		return Response.status(200).entity(ret1).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/auth")
	// @JWTRoleNeeded({"NONE"})
	@EventLog(value = "USER_LOGIN")
	public Response authUser(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String tokenForAuth,
			@QueryParam("p") String p, String payload) {

		byte[] decodedUsername = Base64.getDecoder().decode(p);

		String username = new String(decodedUsername);

		String usr = getUserRepo().getFirstByField(cegId, "email", "STRING", username, false);

		if (usr.length() < 10) {
			return Response.status(401).entity("No user find with this email address.").build();
		}

		Gson gson = new Gson();
		User usrObj = gson.fromJson(usr, User.class);

		if (!usrObj.isActive()) {
			return Response.status(401).entity("User is not active.").build();
		}

		AuthClaims auth = new AuthClaims(("" + usrObj.getId()), cegId, usrObj.getRole(), "login", usrObj.getEmail(),
				usrObj.getName(), usrObj.getUsername());

		String token = Utils.createJwt(auth, (long) (300 * 60 * 1000));

		String u = usr;// .replaceAll("\"","\\\\\"");
		// System.out.println(u);
		return Response.status(200).entity("{\"token\": \"" + token + "\",   \"user\": " + u + "}").build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/jwt")
	public Response createJwt(@QueryParam("exp") Long expMsec, AuthClaims auth) {

		String ret = Utils.createJwt(auth, expMsec);

		return Response.status(200).entity(ret).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users/logout")
	// @JWTRoleNeeded({"NONE"})
	@EventLog(value = "USER_LOGOUT")
	public Response logoutUser(@QueryParam("schema") String schema, @CookieParam("x-access-token") String token,
			@QueryParam("no") String no, @Context HttpHeaders headers, String payload) {

//		AuthClaims auth = Utils.checkAuth(token, headers, true);
//		if (auth == null && !"yes".equals(no)) {
//			return Response.status(401).entity("{\"state\": \"there is no valid auth token.\"}").build();
//		}
//		String actUserId = "999999";
//		String userName = "System";
//		if (auth != null) {
//			actUserId = auth.getId();
//			userName = auth.getName();
//		}

		return Response.status(200).entity("OK").build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/test/{gameId}/{playerId}/move/{x}/{y}")
	// @JWTRoleNeeded({"NONE"})
	@EventLog(value = "USER_LOGOUT")
	public Response testMove(@QueryParam("schema") String schema, @CookieParam("x-access-token") String token,
			@PathParam("gameId") String gameId, @PathParam("playerId") long playerId, @PathParam("x") int x,
			@PathParam("y") int y, @QueryParam("no") String no, @Context HttpHeaders headers, String payload)
			throws JSONException, URISyntaxException {

		StartupListener.move(gameId, playerId, x, y);

		return Response.status(200).entity("OK").build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users/logoutsessionexpired")
	// @JWTRoleNeeded({"NONE"})
	@EventLog(value = "USER_LOGOUT_SESSIONTIMEOUT")
	public Response logoutSessiontimeoutUser(@QueryParam("schema") String schema,
			@CookieParam("x-access-token") String token, @QueryParam("no") String no, @Context HttpHeaders headers,
			String payload) {

		AuthClaims auth = Utils.checkAuth(token, headers);
		if (auth == null && !"yes".equals(no)) {
			return Response.status(401).entity("{\"state\": \"there is no valid auth token.\"}").build();
		}
		String actUserId = "999999";
		String userName = "System";
		if (auth != null) {
			actUserId = auth.getId();
			userName = auth.getName();
		}

		return Response.status(200).entity("OK").build();
	}

	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/jwt/{token}")
	public Response resolveJwt(@PathParam("token") String token) {

		boolean valid = Utils.isJwtValid(token);

		AuthClaims auth = null;
		try {
			auth = Utils.getJwtClaims(token);
		} catch (Exception e) {
			return Response.status(500).entity("{\"valid\":" + valid + ",   \"error\": \"" + e.getMessage() + "\"}")
					.build();
		}

		Gson gson = new Gson();

		String json = gson.toJson(auth);

		return Response.status(200).entity("{\"valid\":" + valid + ",   \"auth\": " + json + "}").build();
	}

}