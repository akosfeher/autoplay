package hu.itbt.autoplay.utils;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import hu.itbt.autoplay.model.AuthClaims;

//@ManagedBean
@JWTRoleNeeded
@Provider
//@Interceptor
public class JWTRoleNeededFilter implements ContainerRequestFilter {

	@Context
	ResourceInfo resourceInfo;

	public JWTRoleNeededFilter() {
		super();
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		MultivaluedMap<String, String> pathparam = requestContext.getUriInfo().getQueryParameters();
		boolean allow = pathparam.get("no") == null || pathparam.get("no").size() == 0 ? false
				: "yes".equals(pathparam.get("no").get(0));

		if (allow) {
			System.out.println("allow no == yes");
			return;
		}
		Set<String> roles = new HashSet<>();
		Class<?> resourceClass = resourceInfo.getResourceClass();
		JWTRoleNeeded classAnnot = resourceClass.getAnnotation(JWTRoleNeeded.class);
		if (classAnnot != null) {
			for (String role : classAnnot.value()) {
				roles.add(role);
			}
			System.out.println("class roles = " + roles);
		}

		Method resourceMethod = resourceInfo.getResourceMethod();
		JWTRoleNeeded methodAnnot = resourceMethod.getAnnotation(JWTRoleNeeded.class);
		if (methodAnnot != null) {
			for (String role : methodAnnot.value()) {
				roles.add(role);
			}

			System.out.println("all roles= " + roles);
		}

		// Get the HTTP Authorization header from the request
		String authorizationHeader = requestContext.getHeaderString("x-access-token");

		if (authorizationHeader == null) {
			try {
				authorizationHeader = requestContext.getCookies().get("x-access-token").getValue();
			} catch (Exception e) {
				authorizationHeader = null;
			}
		}

		if (authorizationHeader == null) {
			System.out.println("#### missing token : ");
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
			return;
		}
		// Extract the token from the HTTP Authorization header
		String token = authorizationHeader.trim();

		try {
			AuthClaims auth = Utils.getAuth(token);
			if (!roles.contains(auth.getRole())) {
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
				return;
			}
		} catch (Exception e) {
			System.out.println("#### invalid token : " + token);
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}
	}
}